import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/constants/user_list_constant.dart';
import 'package:flutterapp/logics/login_logics.dart';
import 'package:flutterapp/models/user_model.dart';
import 'package:flutterapp/pages/main_page.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      // User GestureDetector When have using input keyboard
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: _buildBody,
      ),
    );
  }

  Widget get _buildAppBar {
    return AppBar(
      backgroundColor: Colors.pink,
      centerTitle: true,
      title: Text(
        "Instagram",
        style:
            TextStyle(fontFamily: "Vegas", fontSize: 20, color: Colors.black87),
      ),
    );
  }

  Widget get _buildBody {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.centerLeft,
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            colors: [
              Colors.pinkAccent,
              Colors.deepPurple,
            ]),
      ),
      child: Column(
        children: [
          Expanded(
            child: _buildLoginPanel,
          ),
          _buildSignUp,
        ],
      ),
    );
  }

  Widget get _buildLoginPanel {
    return SingleChildScrollView(
      // IntrinsicHeight follow height child
      child: Container(
        alignment: Alignment.center,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _buildText,
            _buildUsernameTextField,
            _buildPasswordTextField,
            _buildLoginButton,
            _buildForgotLoginTextLink,
            _buildDividerOr,
            _buildLoginWithFacebook,
          ],
        ),
      ),
    );
  }

  Widget get _buildText {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.all(20),
      child: Text(
        "Instagram",
        style:
            TextStyle(fontFamily: "Vegas", fontSize: 20, color: Colors.white),
      ),
    );
  }

  TextEditingController _usernameCtr =
      TextEditingController(text: "rathasao@gmail.com");
  TextEditingController _passwordCtr = TextEditingController(text: "rathasao");

  Widget get _buildUsernameTextField {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(
        horizontal: 20,
      ),
      // constraints max
      constraints: BoxConstraints(
        maxHeight: 50,
        maxWidth: 500,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white38,
      ),
      child: TextField(
        controller: _usernameCtr,
        style: TextStyle(
          fontSize: 20,
          color: Colors.white70,
        ),
        keyboardType: TextInputType.emailAddress,
        autocorrect: false,
        decoration: InputDecoration(
            hintText: "Username",
            hintStyle: TextStyle(
              fontSize: 20,
              color: Colors.white60,
            ),
            border: InputBorder.none),
      ),
    );
  }

  bool _hide = true;

  Widget get _buildPasswordTextField {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(
        horizontal: 20,
      ),
      // constraints max
      constraints: BoxConstraints(
        maxHeight: 50,
        maxWidth: 500,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white38,
      ),
      child: TextField(
        controller: _passwordCtr,
        style: TextStyle(
          fontSize: 20,
          color: Colors.white70,
        ),
        keyboardType: TextInputType.text,
        autocorrect: false,
        decoration: InputDecoration(
          hintText: "Password",
          hintStyle: TextStyle(
            fontSize: 20,
            color: Colors.white60,
          ),
          border: InputBorder.none,
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                _hide = !_hide;
              });
            },
            icon: _hide
                ? Icon(Icons.visibility_off, color: Colors.white)
                : Icon(
                    Icons.visibility,
                    color: Colors.white,
                  ),
          ),
        ),
        obscureText: _hide,
      ),
    );
  }

  Widget get _buildLoginButton {
    return Container(
      // constraints max
      margin: EdgeInsets.symmetric(vertical: 10),
      constraints: BoxConstraints(
        maxHeight: 50,
        maxWidth: 500,
      ),
      width: 500,
      height: 50,
      child: TextButton(
        style: ElevatedButton.styleFrom(
          primary: Colors.white38,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(2),
          ),
        ),
        onPressed: () {
          if (_usernameCtr.text.isEmpty || _passwordCtr.text.isEmpty) {
            _showSnackBar("Username and Password are required");
          } else if (EmailValidator.validate(
                  _usernameCtr.text.toLowerCase().trim()) ==
              false) {
            _showSnackBar("Username and Password are wrong format");
          } else {
            List<UserModel> foundList = userList
                .where((element) =>
                    element.email == _usernameCtr.text.toLowerCase().trim() &&
                    element.password == _passwordCtr.text)
                .toList();
            print(foundList);
            if (foundList.isEmpty) {
              _showSnackBar("Login Failed");
            } else {
              context.read<LoginLogic>().setAddLoggedInUser(foundList[0]);

              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => MainPage()));
            }
          }
        },
        child: Text(
          "Login ",
          style: TextStyle(fontSize: 20, color: Colors.white),
        ),
      ),
    );
  }

  _showSnackBar(String text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("${text}"),
    ));
  }

  Widget get _buildForgotLoginTextLink {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Forgot your login details?",
              style: TextStyle(color: Colors.white),
            ),
            Text(
              "Get help signing in.",
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }

  Widget get _buildDividerOr {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30),
      child: Stack(
        alignment: Alignment.center,
        children: [
          Divider(
            color: Colors.white,
          ),
          Text(
            "OR",
            style: TextStyle(
                fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
          ),
        ],
      ),
    );
  }

  Widget get _buildLoginWithFacebook {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              FontAwesomeIcons.facebookSquare,
              color: Colors.white,
            ),
            SizedBox(
              width: 20,
            ),
            Text(
              "Log in with Facebook",
              style: TextStyle(color: Colors.white, fontSize: 22),
            ),
          ],
        ),
      ),
    );
  }

  Widget get _buildSignUp {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.center,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        physics: NeverScrollableScrollPhysics(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Don't have an account?",
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
            Text(
              "Sign up.",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 17),
            ),
          ],
        ),
      ),
    );
  }
}
