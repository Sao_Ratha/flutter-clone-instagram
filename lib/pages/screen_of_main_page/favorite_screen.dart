import 'package:flutter/material.dart';
import 'package:flutterapp/logics/post_logic.dart';
import 'package:flutterapp/models/post_model.dart';
import 'package:flutterapp/widgets/list__item__post_widget.dart';
import 'package:provider/provider.dart';

class FavoriteScreen extends StatefulWidget {
  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar,
      body: _buildBody,
    );
  }

  AppBar get _buildAppBar {
    return AppBar(
      backgroundColor: Colors.black87,
      centerTitle: true,
      title: Text(
        "Instrgram",
        style: TextStyle(fontFamily: "Vegas", fontSize: 20),
      ),
    );
  }

  Widget get _buildBody {
    return Container(
      alignment: Alignment.center,
      child: _buildListView,
    );
  }

  Widget get _buildListView {
    List<PostModel> favoriteList = context.watch<PostLogic>().favoriteList;

    return Container(
      alignment: Alignment.center,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: favoriteList.length,
        itemBuilder: (context, index) {
          return ListItemPostWidget(
            item: favoriteList[index],
          );
        },
      ),
    );
  }
}
