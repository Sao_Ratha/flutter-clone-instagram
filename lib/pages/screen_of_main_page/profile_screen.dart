import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/constants/post_list_constant.dart';
import 'package:flutterapp/logics/login_logics.dart';
import 'package:flutterapp/models/post_model.dart';
import 'package:flutterapp/models/user_model.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  late UserModel _user;

  @override
  Widget build(BuildContext context) {
    _user = context.watch<LoginLogic>().loggedInUser;

    return Scaffold(
      appBar: _buildAppBar,
      body: _buildBody,
    );
  }

  AppBar get _buildAppBar {
    return AppBar(
      backgroundColor: Colors.grey[50],
      centerTitle: false,
      title: Text(
        "${_user.name}",
        style: TextStyle(color: Colors.black87),
      ),
      actions: [
        IconButton(
            icon: Icon(
              CupertinoIcons.plus_app,
              color: Colors.black87,
            ),
            onPressed: () {}),
        IconButton(
            icon: Icon(
              Icons.menu,
              color: Colors.black87,
            ),
            onPressed: () {}),
      ],
    );
  }

  Widget get _buildBody {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10),
      alignment: Alignment.center,
      child: Column(
        children: [
          _buildRow1,
          Divider(
            color: Colors.grey,
          ),
          _buildPicture,
        ],
      ),
    );
  }

  Widget get _buildRow1 {
    return Container(
      alignment: Alignment.center,
      child: Row(
        children: [
          _buildProfile,
          Container(
            width: 250,
            height: 80,
            //color: Colors.pinkAccent,
            child: Column(
              children: [
                _buildFollow,
                SizedBox(
                  height: 5,
                ),
                _buildEditProfile,
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget get _buildProfile {
    String image = context.select<LoginLogic, String>((logic) => logic.image);
    return CachedNetworkImage(
      imageUrl: image,
      imageBuilder: (context, imageProvider) {
        return Container(
          margin: EdgeInsets.all(5),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 86,
                width: 86,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: [Colors.yellow, Colors.purple],
                  ),
                ),
              ),
              Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                  border: Border.all(color: Colors.white, width: 3),
                ),
              ),
            ],
          ),
        );
      },
      placeholder: (context, url) => Container(
        color: Colors.grey[50],
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  Widget get _buildEditProfile {
    return Container(
      alignment: Alignment.center,
      width: 230,
      height: 30,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(2),
        color: Colors.grey.withOpacity(0.5),
      ),
      child: Text(
        "Edit Profile",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
    );
  }

  Widget get _buildFollow {
    return Column(
      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "128",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  "posts",
                  style: TextStyle(
                      fontSize: 15, color: Colors.grey.withOpacity(0.5)),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  "256",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  "followers",
                  style: TextStyle(
                      fontSize: 15, color: Colors.grey.withOpacity(0.5)),
                ),
              ],
            ),
            Column(
              children: [
                Text(
                  "184",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                Text(
                  "following",
                  style: TextStyle(
                      fontSize: 15, color: Colors.grey.withOpacity(0.5)),
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget get _buildPicture {
    print(_user.id);
    //String image = context.select<LoginLogic, String>((logic) => logic.image);
    List<PostModel> items =
        postList.where((element) => element.userIdFK == _user.id).toList();
    return Container(
      child: GridView.count(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        crossAxisCount: 3,
        mainAxisSpacing: 2,
        crossAxisSpacing: 2,
        children: [
          for (int index = 0; index < items.length; index++)
            Container(
              child: CachedNetworkImage(
                imageUrl: items[index].image,
                placeholder: (context, url) => Container(
                  color: Colors.grey[100],
                ),
                errorWidget: (context, url, error) => Container(
                  color: Colors.grey[400],
                ),
                fit: BoxFit.cover,
              ),
            ),
        ],
      ),
    );
  }
}
