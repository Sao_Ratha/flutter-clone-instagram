import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/constants/post_list_constant.dart';
import 'package:flutterapp/constants/user_list_constant.dart';
import 'package:flutterapp/logics/login_logics.dart';
import 'package:flutterapp/logics/post_logic.dart';
import 'package:flutterapp/models/post_model.dart';
import 'package:flutterapp/pages/screen_of_main_page/favorite_screen.dart';
import 'package:flutterapp/widgets/list__item__post_widget.dart';
import 'package:flutterapp/widgets/story_item_widget.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  //final PostModel item;

  //HomeScreen({required this.item});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String user = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar,
      body: _buildMainListView,
    );
  }

  AppBar get _buildAppBar {
    user = context.read<LoginLogic>().fullName;
    bool isFavoriteEmpty = context.watch<PostLogic>().isFavoriteListEmpty();

    return AppBar(
      backgroundColor: Colors.black87,
      centerTitle: true,
      leading: IconButton(icon: Icon(Icons.camera_alt), onPressed: () {}),
      title: Text(
        "Instrgram ${user}",
        style: TextStyle(fontFamily: "Vegas", fontSize: 20),
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.live_tv),
          onPressed: () {
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => FavoriteScreen()));
          },
        ),
        IconButton(
            icon: Icon(
              FontAwesomeIcons.telegram,
            ),
            onPressed: () {}),
      ],
    );
  }

  Widget get _buildBody {
    return Container(
      alignment: Alignment.center,
      child: _buildMainListView,
    );
  }

  Widget get _buildMainListView {
    return ListView(
      children: [
        _buildStory,
        Divider(
          color: Colors.grey,
        ),
        _buildListView,
      ],
    );
  }

  Widget get _buildListView {
    return Container(
      alignment: Alignment.center,
      child: ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: postList.length,
        itemBuilder: (context, index) {
          return ListItemPostWidget(
            item: postList[index],
          );
        },
      ),
    );
  }

  Widget get _buildStory {
    return Container(
      alignment: Alignment.center,
      height: 120,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: userList.length,
          itemBuilder: (context, index) {
            return StoryItemWidget(
              item: userList[index],
            );
          }),
    );
  }
}
