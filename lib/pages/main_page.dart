import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/logics/login_logics.dart';
import 'package:flutterapp/logics/post_logic.dart';
import 'package:flutterapp/pages/screen_of_main_page/addImageScreen.dart';
import 'package:flutterapp/pages/screen_of_main_page/favorite_screen.dart';
import 'package:flutterapp/pages/screen_of_main_page/home_screen.dart';
import 'package:flutterapp/pages/screen_of_main_page/profile_screen.dart';
import 'package:flutterapp/pages/screen_of_main_page/search_screen.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

//  with AutomaticKeepAliveClientMixin for member location
class _MainPageState extends State<MainPage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody,
      bottomNavigationBar: _buildBottom,
    );
  }

  HomeScreen _homeScreen = HomeScreen();
  SearchScreen _searchScreen = SearchScreen();
  AddImageScreen _addImageScreen = AddImageScreen();
  FavoriteScreen _favoriteScreen = FavoriteScreen();
  ProfileScreen _profileScreen = ProfileScreen();

  late List<Widget> _pageList = [
    _homeScreen,
    _searchScreen,
    _addImageScreen,
    _favoriteScreen,
    _profileScreen,
  ];

  int _currentIndex = 0;
  PageController _pageController = PageController();

  Widget get _buildBody {
    return Container(
      alignment: Alignment.center,
      child: PageView(
        controller: _pageController,
        physics: NeverScrollableScrollPhysics(),
        onPageChanged: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        children: _pageList,
      ),
    );
  }

  Widget get _buildBottom {
    bool isFavoriteEmpty = context.watch<PostLogic>().isFavoriteListEmpty();

    return BottomNavigationBar(
      selectedItemColor: Colors.red,
      unselectedItemColor: Colors.black87,
      currentIndex: _currentIndex,
      onTap: (index) {
        _pageController.jumpToPage(index);
        /*
        _pageController.animateToPage(
          index,
          duration: Duration(milliseconds: 200),
          curve: Curves.easeInOut,
        );
         */
      },
      items: [
        BottomNavigationBarItem(
          icon: Icon(
            CupertinoIcons.home,
            color: Colors.black87,
            size: 30,
          ),
          label: "",
        ),
        BottomNavigationBarItem(
          icon: Icon(
            CupertinoIcons.search,
            color: Colors.black87,
            size: 30,
          ),
          label: "",
        ),
        BottomNavigationBarItem(
          icon: Icon(
            CupertinoIcons.plus_app,
            color: Colors.black87,
            size: 30,
          ),
          label: "",
        ),
        BottomNavigationBarItem(
          icon: isFavoriteEmpty
              ? Icon(CupertinoIcons.heart)
              : Icon(
                  CupertinoIcons.heart_fill,
                  color: Colors.red,
                ),
          label: "",
        ),
        BottomNavigationBarItem(icon: _buildPicture, label: ""),
      ],
    );
  }

  Widget get _buildPicture {
    String image = context.select<LoginLogic, String>((logic) => logic.image);
    return CachedNetworkImage(
      imageUrl: image,
      imageBuilder: (context, imageProvider) {
        return Container(
          margin: EdgeInsets.all(5),
          child: Stack(
            alignment: Alignment.center,
            children: [
              Container(
                height: 36,
                width: 36,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  gradient: LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: [Colors.yellow, Colors.purple],
                  ),
                ),
              ),
              Container(
                width: 30,
                height: 30,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: imageProvider,
                    fit: BoxFit.cover,
                  ),
                  border: Border.all(color: Colors.white, width: 3),
                ),
              ),
            ],
          ),
        );
      },
      placeholder: (context, url) => Container(
        color: Colors.grey[50],
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }

  // Member location
  @override
  bool get wantKeepAlive => true;
}
