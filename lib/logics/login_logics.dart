import 'package:flutter/foundation.dart';
import 'package:flutterapp/models/user_model.dart';

class LoginLogic extends ChangeNotifier {

  late UserModel _loginUser;

  UserModel get loggedInUser => _loginUser;

  void setAddLoggedInUser(UserModel userModel) {
    _loginUser = userModel;
    notifyListeners();
  }

  String get fullName {
    return _loginUser.name;
  }

  String get image {
    return _loginUser.image;
  }
}
