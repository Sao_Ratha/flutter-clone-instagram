class PostModel {
  int postId;
  int userIdFK;
  String caption;
  String image;
  int likeCount;

  PostModel({
    this.postId = 1,
    this.userIdFK = 1,
    this.caption = "no-title",
    this.image = "no-image",
    this.likeCount = 0,
  });
}
