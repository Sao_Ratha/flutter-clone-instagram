class UserModel {
  int id;
  String name;
  String email;
  String password;
  String image;
  String tel;

  UserModel(
      {this.id = 1,
      this.name = "no-name",
      this.email = "no-email",
      this.password = "no-password",
      this.image = "no-image",
      this.tel = "tel"});
}
