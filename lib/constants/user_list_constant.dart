import 'package:flutterapp/models/user_model.dart';

List<UserModel> userList = [
  UserModel(
      id: 101,
      name: "AAA BBB CCC DDD EEE",
      email: "BBB@gmail.com",
      password: "pb123",
      tel: "01346363633",
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQVupuN1mxZ4idrdir0CNNAJCfJyBQFGJ2WoauUL-XhXJhMcOmuZ0ESeqBxqcE0FlbV6C4&usqp=CAU"),
  UserModel(
      id: 102,
      name: "CCC",
      email: "CCC@gmail.com",
      password: "pc123",
      tel: "01446363633",
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWOC9WHm-VmyZvFvHbztXNDRCiOnp5UENbpOSBEirRml4g71KdpCJv7_G0Gy0H1HNklow&usqp=CAU"),
  UserModel(
      id: 103,
      name: "DDD",
      email: "DDD@gmail.com",
      password: "pd123",
      tel: "01546363633",
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRE1_hQ6BEfpFTOuaNDIugTss3woa1ZZfH_MboYXZo0i042izjlnDqe22Qzmp6vd6gqgZI&usqp=CAU"),
  UserModel(
      id: 104,
      name: "EEE",
      email: "EEE@gmail.com",
      password: "pe123",
      tel: "01646363633",
      image:
          "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQy6XqQvGKo4zbTZDg8TZ0LlFNLPZKcf8uI8XZbcghuLK5MV8yuaqr7_9GSHvGAvBQ4ZSc&usqp=CAU"),
  UserModel(
      id: 105,
      name: "FFF",
      email: "FFF@gmail.com",
      password: "pf123",
      tel: "01846363633",
      image:
          "https://1.bp.blogspot.com/--t9fKmbPcOY/UJL7jUjXC-I/AAAAAAAAP8s/h8mPcidhINM/s400/03a.jpg"),
  UserModel(
      id: 106,
      name: "Sao Ratha",
      email: "rathasao@gmail.com",
      password: "rathasao",
      tel: "01246363633",
      image:
          "https://pbs.twimg.com/profile_images/814966947360059392/NSUrMtb8_400x400.jpg"),
];
