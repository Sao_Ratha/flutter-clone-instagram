import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutterapp/logics/login_logics.dart';
import 'package:flutterapp/logics/post_logic.dart';
import 'package:flutterapp/pages/login_page.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => LoginLogic(),
        ),
        ChangeNotifierProvider(
          create: (context) => PostLogic(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: LoginPage(),
      ),
    );
  }
}
