import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterapp/constants/user_list_constant.dart';
import 'package:flutterapp/logics/post_logic.dart';
import 'package:flutterapp/models/post_model.dart';
import 'package:flutterapp/models/user_model.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class ListItemPostWidget extends StatefulWidget {
  final PostModel item;

  ListItemPostWidget({required this.item});

  @override
  _ListItemPostWidgetState createState() => _ListItemPostWidgetState();
}

class _ListItemPostWidgetState extends State<ListItemPostWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Column(
        children: [
          _buildRowProfile,
          _buildRowPicture,
          _buildRowComment,
          _buildRowLike,
          _buildRowCaption,
          Divider(
            color: Colors.grey,
          ),
        ],
      ),
    );
  }

  Widget get _buildRowProfile {
    UserModel user =
        userList.where((element) => element.id == widget.item.userIdFK).single;

    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              CachedNetworkImage(
                imageUrl: user.image,
                imageBuilder: (context, imageProvider) {
                  return Container(
                    margin: EdgeInsets.all(5),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          height: 40,
                          width: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                              colors: [Colors.yellow, Colors.purple],
                            ),
                          ),
                        ),
                        Container(
                          width: 35,
                          height: 35,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: imageProvider,
                              fit: BoxFit.cover,
                            ),
                            border: Border.all(color: Colors.white, width: 3),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                placeholder: (context, url) => Container(
                  color: Colors.grey[50],
                ),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              Container(
                width: 200,
                child: Text(
                  "${user.name}",
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
          IconButton(icon: Icon(Icons.more_horiz), onPressed: () {}),
        ],
      ),
    );
  }

  Widget get _buildRowPicture {
    return Container(
      alignment: Alignment.center,
      child: CachedNetworkImage(
        imageUrl: widget.item.image,
        placeholder: (context, url) => Container(
          color: Colors.grey[50],
        ),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );
  }

  Widget get _buildRowComment {
    bool isFavorite = context.watch<PostLogic>().isFavorite(widget.item);

    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: Row(
              children: [
                IconButton(
                  icon: isFavorite
                      ? Icon(
                          CupertinoIcons.heart_fill,
                          color: Colors.red,
                        )
                      : Icon(
                          CupertinoIcons.heart,
                        ),
                  onPressed: () {
                    setState(() {
                      if (isFavorite) {
                        context
                            .read<PostLogic>()
                            .removePostFromFavorite(widget.item);
                      } else {
                        context
                            .read<PostLogic>()
                            .addPostToFavorite(widget.item);
                      }
                    });
                  },
                ),
                IconButton(
                    icon: Icon(FontAwesomeIcons.circle), onPressed: () {}),
                IconButton(
                    icon: Icon(FontAwesomeIcons.telegram), onPressed: () {})
              ],
            ),
          ),
          IconButton(
              icon: Icon(
                Icons.bookmark_border,
              ),
              onPressed: () {}),
        ],
      ),
    );
  }

  Widget get _buildRowLike {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      alignment: Alignment.centerLeft,
      child: Text("${widget.item.likeCount} likes"),
    );
  }

  Widget get _buildRowCaption {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      alignment: Alignment.centerLeft,
      child: Text("${widget.item.caption}"),
    );
  }
}
